﻿namespace Tables_1
{
    partial class Start
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        //private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Start));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.Discount = new System.Windows.Forms.ToolStripMenuItem();
            this.Discount23 = new System.Windows.Forms.ToolStripMenuItem();
            this.Discount15 = new System.Windows.Forms.ToolStripMenuItem();
            this.CatalogPrice = new System.Windows.Forms.ToolStripMenuItem();
            this.Storage = new System.Windows.Forms.ToolStripMenuItem();
            this.AddItems = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteItems = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenDocs = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenDocument = new System.Windows.Forms.ToolStripMenuItem();
            this.Arrears = new System.Windows.Forms.ToolStripMenuItem();
            this.lstArrears = new System.Windows.Forms.ToolStripMenuItem();
            this.RealSum = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateReturns = new System.Windows.Forms.ToolStripMenuItem();
            this.Info = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Discount,
            this.Storage,
            this.OpenDocs,
            this.Arrears,
            this.RealSum,
            this.Info});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1005, 38);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip";
            // 
            // Discount
            // 
            this.Discount.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Discount23,
            this.Discount15,
            this.CatalogPrice});
            this.Discount.Font = new System.Drawing.Font("Segoe UI", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Discount.ForeColor = System.Drawing.Color.Navy;
            this.Discount.Name = "Discount";
            this.Discount.Size = new System.Drawing.Size(99, 34);
            this.Discount.Text = "Скидка";
            // 
            // Discount23
            // 
            this.Discount23.ForeColor = System.Drawing.Color.Navy;
            this.Discount23.Name = "Discount23";
            this.Discount23.Size = new System.Drawing.Size(247, 34);
            this.Discount23.Text = "23%";
            this.Discount23.Click += new System.EventHandler(this.Discount23_Click);
            // 
            // Discount15
            // 
            this.Discount15.ForeColor = System.Drawing.Color.Navy;
            this.Discount15.Name = "Discount15";
            this.Discount15.Size = new System.Drawing.Size(247, 34);
            this.Discount15.Text = "15%";
            this.Discount15.Click += new System.EventHandler(this.Discount15_Click);
            // 
            // CatalogPrice
            // 
            this.CatalogPrice.ForeColor = System.Drawing.Color.Navy;
            this.CatalogPrice.Name = "CatalogPrice";
            this.CatalogPrice.Size = new System.Drawing.Size(247, 34);
            this.CatalogPrice.Text = "Цена каталога";
            this.CatalogPrice.Click += new System.EventHandler(this.CatalogPrice_Click);
            // 
            // Storage
            // 
            this.Storage.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddItems,
            this.DeleteItems});
            this.Storage.Font = new System.Drawing.Font("Segoe UI", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Storage.ForeColor = System.Drawing.Color.Navy;
            this.Storage.Name = "Storage";
            this.Storage.Size = new System.Drawing.Size(87, 34);
            this.Storage.Text = "Склад";
            // 
            // AddItems
            // 
            this.AddItems.ForeColor = System.Drawing.Color.Navy;
            this.AddItems.Name = "AddItems";
            this.AddItems.Size = new System.Drawing.Size(163, 34);
            this.AddItems.Text = "Приход";
            this.AddItems.Click += new System.EventHandler(this.AddItems_Click);
            // 
            // DeleteItems
            // 
            this.DeleteItems.ForeColor = System.Drawing.Color.Navy;
            this.DeleteItems.Name = "DeleteItems";
            this.DeleteItems.Size = new System.Drawing.Size(163, 34);
            this.DeleteItems.Text = "Расход";
            this.DeleteItems.Click += new System.EventHandler(this.DeleteItems_Click);
            // 
            // OpenDocs
            // 
            this.OpenDocs.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenDocument});
            this.OpenDocs.Font = new System.Drawing.Font("Segoe UI", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.OpenDocs.ForeColor = System.Drawing.Color.Navy;
            this.OpenDocs.Name = "OpenDocs";
            this.OpenDocs.Size = new System.Drawing.Size(143, 34);
            this.OpenDocs.Text = "Накладные";
            // 
            // OpenDocument
            // 
            this.OpenDocument.ForeColor = System.Drawing.Color.Navy;
            this.OpenDocument.Name = "OpenDocument";
            this.OpenDocument.Size = new System.Drawing.Size(193, 34);
            this.OpenDocument.Text = "Открыть";
            this.OpenDocument.Click += new System.EventHandler(this.OpenDocument_Click);
            // 
            // Arrears
            // 
            this.Arrears.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lstArrears});
            this.Arrears.Font = new System.Drawing.Font("Segoe UI", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Arrears.ForeColor = System.Drawing.Color.Navy;
            this.Arrears.Name = "Arrears";
            this.Arrears.Size = new System.Drawing.Size(89, 34);
            this.Arrears.Text = "Долги";
            // 
            // lstArrears
            // 
            this.lstArrears.ForeColor = System.Drawing.Color.Navy;
            this.lstArrears.Name = "lstArrears";
            this.lstArrears.Size = new System.Drawing.Size(235, 34);
            this.lstArrears.Text = "Список долгов";
            this.lstArrears.Click += new System.EventHandler(this.lstArrears_Click);
            // 
            // RealSum
            // 
            this.RealSum.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateReturns});
            this.RealSum.Font = new System.Drawing.Font("Segoe UI", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.RealSum.ForeColor = System.Drawing.Color.Navy;
            this.RealSum.Name = "RealSum";
            this.RealSum.Size = new System.Drawing.Size(132, 34);
            this.RealSum.Text = "Возвраты";
            // 
            // CreateReturns
            // 
            this.CreateReturns.ForeColor = System.Drawing.Color.Navy;
            this.CreateReturns.Name = "CreateReturns";
            this.CreateReturns.Size = new System.Drawing.Size(176, 34);
            this.CreateReturns.Text = "Создать";
            this.CreateReturns.Click += new System.EventHandler(this.CreateReturns_Click);
            // 
            // Info
            // 
            this.Info.Font = new System.Drawing.Font("Segoe UI", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Info.ForeColor = System.Drawing.Color.Navy;
            this.Info.Name = "Info";
            this.Info.Size = new System.Drawing.Size(110, 34);
            this.Info.Text = "Справка";
            this.Info.Click += new System.EventHandler(this.Info_Click);
            // 
            // Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.BackgroundImage = global::Tables_1.Properties.Resources.z;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1005, 561);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Start";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Amway";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Start_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem Storage;
        private System.Windows.Forms.ToolStripMenuItem AddItems;
        private System.Windows.Forms.ToolStripMenuItem OpenDocs;
        private System.Windows.Forms.ToolStripMenuItem OpenDocument;
        private System.Windows.Forms.ToolStripMenuItem Arrears;
        private System.Windows.Forms.ToolStripMenuItem lstArrears;
        private System.Windows.Forms.ToolStripMenuItem RealSum;
        private System.Windows.Forms.ToolStripMenuItem CreateReturns;
        public System.Windows.Forms.MenuStrip menuStrip1;
        public System.Windows.Forms.ToolStripMenuItem Discount23;
        public System.Windows.Forms.ToolStripMenuItem Discount15;
        public System.Windows.Forms.ToolStripMenuItem CatalogPrice;
        private System.Windows.Forms.ToolStripMenuItem Discount;
        private System.Windows.Forms.ToolStripMenuItem DeleteItems;
        private System.Windows.Forms.ToolStripMenuItem Info;
    }
}

