﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tables_1
{
    public partial class FormGeneralTable : Form
    {
        public System.Windows.Forms.DataGridView Doc1;
        public List<DataBase> DataGrid = new List<DataBase>();
        public FormGeneralTable()
        {
            InitializeComponent();
        }
        #region Заполение таблицы 
        public void Document1_KeyUp(object sender, KeyEventArgs e)
        {
            Start Menu = new Start();
            for (int i = 0; i < Document1.RowCount; i++)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (Document1[0, i] != null)
                    {
                        var artikul = Document1[0, i].Value;
                        var count = Document1[2, i].Value;

                        if (artikul != null && (string)artikul != "")
                        {
                            var answer = Menu.MyData.Data.Find(x => x.Artikul.Equals(artikul.ToString()));
                            if (answer != null)
                            {
                                Document1["Product", i].Value = answer.Name;
                                if (txtDiscount.Text != "Склад")
                                {
                                    Document1["Count", i].Value = 1;
                                    Document1["Points", i].Value = answer.Ball;
                                    Document1["Storage", i].Value = answer.Sklad;
                                }
                                if (txtDiscount.Text.Contains("23%"))
                                {
                                    Document1["Sum", i].Value = answer.buy23;
                                }
                                if (txtDiscount.Text.Contains("15%"))
                                {
                                    Document1["Sum", i].Value = answer.buy15;
                                }
                                if (!txtDiscount.Text.Contains("15%") && !txtDiscount.Text.Contains("23%") && txtDiscount.Text != "Склад")
                                {
                                    Document1["Sum", i].Value = answer.buy100;
                                }
                                if (count != null)
                                {
                                    bool flag = true;
                                    string RealCount = count.ToString();
                                    for (int j = 0; j < RealCount.Length; j++)
                                    {
                                        if (Char.IsDigit(RealCount[j]))
                                        {
                                            continue;
                                        }
                                        else
                                        {
                                            flag = false;
                                            Document1["Count", i].Value = 1;
                                            MessageBox.Show("Неверный формат количества");
                                            break;
                                        }
                                    }
                                        if (txtDiscount.Text != "Склад" && flag)
                                        {
                                            Document1["Count", i].Value = count;
                                            Document1["Points", i].Value = answer.Ball * int.Parse(count.ToString());
                                            Document1["Storage", i].Value = answer.Sklad;
                                        }
                                        if (!txtDiscount.Text.Contains("15%") && !txtDiscount.Text.Contains("23%") && txtDiscount.Text != "Склад" && flag)
                                        {
                                            Document1["Sum", i].Value = answer.buy100 * int.Parse(count.ToString());
                                        }
                                        if (txtDiscount.Text.Contains("15%") && flag)
                                        {
                                            Document1["Sum", i].Value = answer.buy15 * int.Parse(count.ToString());
                                        }
                                        if (txtDiscount.Text.Contains("23%") && flag)
                                        {
                                            Document1["Sum", i].Value = answer.buy23 * int.Parse(count.ToString());
                                        }
                                    }

                                    if (Document1["Count", i].Value != null && Document1["Count", i].Value.ToString() != "" && Goods.Text == "Расход")
                                    {
                                        if (answer.Sklad < int.Parse(Document1["Count", i].Value.ToString()))
                                        {
                                            MessageBox.Show("На складе недостаточно товара. Невозможно произвести расход");
                                            Document1["Count", i].Value = "";
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Артикул не найден!");
                                    Document1["Number", i].Value = "";
                                }
                            }
                        }
                    }
                }
            }
        
        #endregion

        #region Выбор цены
        public List<DataBase> Amounts1()
        {
            DataGrid = new List<DataBase>();
            int i = 0;
                for (int j = 0; j < Document1.RowCount; j++)
                {
                    int Arrear = 0;
                    if ((Document1[i + 6, j].Value) != null) Arrear = int.Parse(Document1[i + 6, j].Value.ToString());
                 
                    if ((string)Document1[i, j].Value != null && (string)Document1[i, j].Value != "" && txtDiscount.Text.Contains("23%"))
                    {
                        DataGrid.Add(new DataBase((string)Document1[i, j].Value, (string)Document1[i + 1, j].Value, Document1[i + 2, j].Value.ToString(),
                            0, 0, (decimal)Document1[i + 3, j].Value, 0, (decimal)Document1[i + 4, j].Value, (int)Document1[i + 5, j].Value, Arrear));
                    }

                    if ((string)Document1[i, j].Value != null && (string)Document1[i, j].Value != "" && txtDiscount.Text.Contains("15%"))
                    {
                        DataGrid.Add(new DataBase((string)Document1[i, j].Value, (string)Document1[i + 1, j].Value, Document1[i + 2, j].Value.ToString(),
                            0, (decimal)Document1[i + 3, j].Value, 0, 0, (decimal)Document1[i + 4, j].Value, (int)Document1[i + 5, j].Value, Arrear));
                    }

                    if ((string)Document1[i, j].Value != null && (string)Document1[i, j].Value != "" && !txtDiscount.Text.Contains("23%") && !txtDiscount.Text.Contains("15%") && txtDiscount.Text != "Склад")
                    {
                        DataGrid.Add(new DataBase((string)Document1[i, j].Value, (string)Document1[i + 1, j].Value, Document1[i + 2, j].Value.ToString(),
                            (decimal)Document1[i + 3, j].Value, 0, 0, 0, (decimal)Document1[i + 4, j].Value, (int)Document1[i + 5, j].Value, Arrear));
                    }
                }
            return DataGrid;
        }
        public List<DataBase> Doc2 = new List<DataBase>();
        public void MyResult_Click(object sender, EventArgs e) 
        {
            var Doc1 = Amounts1();
            Doc2 = Doc1;
            Amount MyAmount = new Amount();
            double points = MyAmount.Summa(Doc1);
            WithoutNalog.Text = points.ToString();
            double percent = points * 1.05;
            WithNalog.Text = percent.ToString();
            Save.Visible = true;
            txtPoints.Text = Points().ToString();
        }
        #endregion

        private void Save_Click(object sender, EventArgs e)
        {

            Save.Visible = false;
            if (txtName.Text != "" && txtNumber.Text != "")
            {
                Saving SaveMe = new Saving();
                SaveMe.CreateFile(Doc2, txtName.Text, txtNumber.Text, txtDate.Text, WithNalog.Text, txtPoints.Text);
                ListArrears Arrear = new ListArrears();
                Arrear.ListOfArrears(Doc2, txtName.Text, txtNumber.Text, txtDate.Text, WithNalog.Text);
                MessageBox.Show("Файл успешно создан, данные обновлены");
            }

            else MessageBox.Show("Введены не все данные!");
        }

     

        private void buttonClear_Click(object sender, EventArgs e)
        {
            WithoutNalog.Text = "";
            WithNalog.Text = "";
            txtName.Text = "";
            txtNumber.Text = "";
            txtPoints.Text = "";
            for (int i = 0; i < Document1.Rows.Count; i++)
            {

                Document1["Product", i].Value = null;
                Document1["Count", i].Value = null;
                Document1["Sum", i].Value = null;
                Document1["Number", i].Value = null;
                Document1["Points", i].Value = null;
                Document1["Storage", i].Value = null;
                Document1["Arrears", i].Value = null;
            }
        }

        private void FormGeneralTable_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
                Start t = new Start();
                t.Show();
            }
        }

        #region Приход/Расход
        private void SaveSklad_Click(object sender, EventArgs e)
        {
            bool flag = true;
            Storage store = new Storage();
            Start Menu = new Start();
            DataGrid = store.AddGoods();
            for (int i = 0; i < 15; i++)
            {
                if (Document1[0, i] != null)
                {
                    var artikul = Document1[0, i].Value;
                    if (Document1["Count", i].Value != null)
                    {
                        try
                        {
                            var answer = Menu.MyData.Data.Find(x => x.Artikul.Equals(artikul.ToString()));
                            for (int j = 0; j < DataGrid.Count; j++)
                            {
                                if (DataGrid[j].Artikul == answer.Artikul)
                                {
                                    int k = int.Parse(Document1["Count", i].Value.ToString());
                                    if (Goods.Text == "Приход")
                                    {
                                        DataGrid[j].Sklad = DataGrid[j].Sklad + k;

                                    }
                                    if (Goods.Text == "Расход")
                                    {
                                        DataGrid[j].Sklad = DataGrid[j].Sklad - k;
                                    }
                                    break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Таблица составлена некорректно!");
                            flag = false;
                            break;
                        }
                    }
                }
            }
            if (flag && Document1["Number", 0].Value != null)
            {
                store.SaveIt(DataGrid);
                MessageBox.Show("Данные успешно обновлены!");
            }
            else MessageBox.Show("Заполните таблицу корректно!");
        }
        #endregion

        #region Баллы
        public double Points()
        {
            double sum = 0;
            for(int i = 0; i<Document1.RowCount; i++)
            {
                if (Document1["Points", i].Value != null)
                {
                    sum += Convert.ToDouble(Document1["Points", i].Value.ToString());
                }
            }
            return sum;
        }
        #endregion
    }
}
