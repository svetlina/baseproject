﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;
using Word = Microsoft.Office.Interop.Word;
using System.IO;

namespace Tables_1
{
   
    public partial class Start : Form
    {
        public Table1 TBL = new Table1();
        public AllData MyData = new AllData();
        public Start()
        {
            TBL.GTable = TBL.CreateTable();
            MyData.Data = TBL.ReadingCatalog();
            InitializeComponent();
           
        }

        public void ShowTable()
        {
            TBL = new Table1();
            TBL.CreateTable();
            TBL.GTable.Show();
            TBL.GTable.txtDiscount.Visible = true;
            TBL.GTable.txtDiscount.ForeColor = Color.Red;
        }

        #region Кнопки меню
        private void Discount23_Click(object sender, EventArgs e)
        {
            Amount amount = new Amount();
            ShowTable();
            TBL.GTable.txtDiscount.Text = "Продукция со скидкой 23%";
            TBL.GTable.txtDate.Text = DateTime.Today.ToShortDateString();
            TBL.GTable.KeyUp += TBL.GTable.Document1_KeyUp;
            TBL.GTable.Goods.Text = "";
            this.Hide();
        }

        private void Discount15_Click(object sender, EventArgs e)
        {
            ShowTable();
            TBL.GTable.txtDiscount.Text = "Продукция со скидкой 15%";
            TBL.GTable.txtDate.Text = DateTime.Today.ToShortDateString();
            TBL.GTable.KeyUp += TBL.GTable.Document1_KeyUp;
            TBL.GTable.Goods.Text = "";
            this.Hide();
        }

        private void CatalogPrice_Click(object sender, EventArgs e)
        {
            ShowTable();
            TBL.GTable.txtDiscount.Text = "Продукция по цене каталога";
            TBL.GTable.txtDate.Text = DateTime.Today.ToShortDateString();
            TBL.GTable.KeyUp += TBL.GTable.Document1_KeyUp;
            TBL.GTable.Goods.Text = "";
            this.Hide();
        }

        private void AddItems_Click(object sender, EventArgs e)
        {
            Storage Store = new Storage();
            Store.MyBase();
            Store.GTable.Save.Visible = false;
            Store.GTable.lblName.Visible = false;
            Store.GTable.txtName.Visible = false;
            Store.GTable.Number.Visible = false;
            Store.GTable.txtNumber.Visible = false;
            Store.GTable.Date.Visible = false;
            Store.GTable.txtDate.Visible = false;
            Store.GTable.SaveSklad.Visible = true;
            Store.GTable.Result.Visible = false;
            Store.GTable.Plus.Visible = false;
            Store.GTable.WithoutNalog.Visible = false;
            Store.GTable.WithNalog.Visible = false;
            Store.GTable.MyResult.Visible = false;
            Store.GTable.txtDiscount.Text = "Склад";
            Store.GTable.txtDiscount.Visible = false;
            for (int i = 0; i < TBL.GTable.Document1.Rows.Count; i++)
            {
                TBL.GTable.Document1["Product", i].Value = null;
                TBL.GTable.Document1["Count", i].Value = null;
                TBL.GTable.Document1["Number", i].Value = null;
            }
            Store.GTable.txtPoints.Visible = false;
            Store.GTable.lblPoints.Visible = false;
            Store.GTable.buttonClear.Visible = false;
            Store.GTable.Show();
            Store.GTable.Goods.Text = "Приход";
            Store.GTable.Goods.Visible = true;
            this.Hide();
        }

        private void DeleteItems_Click(object sender, EventArgs e)
        {
            Storage Store = new Storage();
            Store.MyBase();
            Store.GTable.Save.Visible = false;
            Store.GTable.lblName.Visible = false;
            Store.GTable.txtName.Visible = false;
            Store.GTable.Number.Visible = false;
            Store.GTable.txtNumber.Visible = false;
            Store.GTable.Date.Visible = false;
            Store.GTable.txtDate.Visible = false;
            Store.GTable.SaveSklad.Visible = true;
            Store.GTable.Result.Visible = false;
            Store.GTable.Plus.Visible = false;
            Store.GTable.WithoutNalog.Visible = false;
            Store.GTable.WithNalog.Visible = false;
            Store.GTable.MyResult.Visible = false;
            Store.GTable.txtDiscount.Text = "Склад";
            Store.GTable.txtDiscount.Visible = false;
            for (int i = 0; i < TBL.GTable.Document1.Rows.Count; i++)
            {
                TBL.GTable.Document1["Product", i].Value = null;
                TBL.GTable.Document1["Count", i].Value = null;
                TBL.GTable.Document1["Number", i].Value = null;
            }
            Store.GTable.txtPoints.Visible = false;
            Store.GTable.lblPoints.Visible = false;
            Store.GTable.buttonClear.Visible = false;
            Store.GTable.Show();
            Store.GTable.Goods.Text = "Расход";
            Store.GTable.Goods.Visible = true;
            this.Hide();
        }

        private void OpenDocument_Click(object sender, EventArgs e)
        {
            OpenDoc Doc = new OpenDoc();
            Doc.Show();
            this.Hide();
        }

        private void lstArrears_Click(object sender, EventArgs e)
        {
            ListArrears lstArrears = new ListArrears();
            lstArrears.Arrears();
            lstArrears.GTable.Show();
            lstArrears.GTable.Save.Visible = false;
            lstArrears.GTable.lblName.Visible = false;
            lstArrears.GTable.txtName.Visible = false;
            lstArrears.GTable.Number.Visible = false;
            lstArrears.GTable.txtNumber.Visible = false;
            lstArrears.GTable.Date.Visible = false;
            lstArrears.GTable.txtDate.Visible = false;
            lstArrears.GTable.SaveSklad.Visible = false;
            lstArrears.GTable.Result.Visible = false;
            lstArrears.GTable.Plus.Visible = false;
            lstArrears.GTable.WithoutNalog.Visible = false;
            lstArrears.GTable.WithNalog.Visible = false;
            lstArrears.GTable.MyResult.Visible = false;
            lstArrears.GTable.Goods.Visible = true;
            lstArrears.GTable.buttonClear.Visible = false;
            lstArrears.GTable.txtPoints.Visible = false;
            lstArrears.GTable.lblPoints.Visible = false;
            lstArrears.GTable.Goods.Text = "Долги";
            List<NPA> MyBase = lstArrears.GetArrears();
            for (int i = 0; i < MyBase.Count; i++)
            {
                lstArrears.GTable.Document1["Number", i].Value = MyBase[i].Number;
                lstArrears.GTable.Document1["Name", i].Value = MyBase[i].Name;
                lstArrears.GTable.Document1["Artikul", i].Value = MyBase[i].Artikul;
                lstArrears.GTable.Document1["Product", i].Value = MyBase[i].Product;
                lstArrears.GTable.Document1["Count", i].Value = MyBase[i].Arrear;
            }
            this.Hide();
        }

        private void CreateReturns_Click(object sender, EventArgs e)
        {
            CreateDoc Doc = new CreateDoc();
            Doc.Show();
            this.Hide();
        }

        private void Start_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Environment.Exit(0);
            }
        }

        private void Info_Click(object sender, EventArgs e)
        {
            Information Info = new Information();
            Info.Reading();
        }

    }
        #endregion

    public class AllData
    {
        public List<DataBase> Data = new List<DataBase>();
    }
    public class Creator
    {
        public DataGridView Gen = new DataGridView();
        public DataGridViewColumn column1 = new DataGridViewColumn();
        public DataGridViewColumn column2 = new DataGridViewColumn();
        public DataGridViewColumn column3 = new DataGridViewColumn();
        public DataGridViewColumn column4 = new DataGridViewColumn();
        public DataGridViewColumn column5 = new DataGridViewColumn();
        public DataGridViewColumn column6 = new DataGridViewColumn();
        public DataGridViewColumn column7 = new DataGridViewColumn();
        public void Create()
        {
            column1.CellTemplate = new DataGridViewTextBoxCell();
            column2.CellTemplate = new DataGridViewTextBoxCell();
            column3.CellTemplate = new DataGridViewTextBoxCell();
            column4.CellTemplate = new DataGridViewTextBoxCell();
            column5.CellTemplate = new DataGridViewTextBoxCell();
            column6.CellTemplate = new DataGridViewTextBoxCell();
            column7.CellTemplate = new DataGridViewTextBoxCell();
        }
    }
}
