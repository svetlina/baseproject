﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tables_1
{
    public partial class CreateDoc : Form
    {
        public CreateDoc()
        {
            InitializeComponent();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            Returns Doc = new Returns();
            if (txtNumber.Text != "" && txtName.Text != "" && Summa.Text != "" && RealSumma.Text != "" && ADiscount.Text != "" && Discount.Text != "" && Return.Text != "" && txtData.Text != "")
            {
                Doc.ReturnsTable(txtNumber.Text, txtName.Text, Summa.Text, RealSumma.Text, ADiscount.Text, Discount.Text, Return.Text, txtData.Text);
                Save.Visible = false;
                MessageBox.Show("Данные успешно записаны!");
            }
            else MessageBox.Show("Введены не все данные!");
        }

        private void Calculate_Click(object sender, EventArgs e)
        {
            try
            {
                double itog = int.Parse(Summa.Text) - ((int.Parse(RealSumma.Text) + int.Parse(ADiscount.Text) + int.Parse(Discount.Text)) * 1.05) - int.Parse(ADiscount.Text) - int.Parse(Discount.Text);
                if (itog < 0)
                {
                    Return.Text = itog.ToString();
                }
                else
                {
                    Return.Text = itog.ToString();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Неверный формат данных");
            }
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            Save.Visible = true;
            Summa.Text = "";
            RealSumma.Text = "";
            ADiscount.Text = "";
            Discount.Text = "";
            Return.Text = "";
            txtName.Text = "";
            txtNumber.Text = "";
        }

        private void Returns_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
                Start t = new Start();
                t.Show();
            }
        }
    }
}
