﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using System.Diagnostics;

namespace Tables_1
{
    public partial class OpenDoc : Form
    {
        public Table1 TBL = new Table1();
        public OpenDoc()
        {
            TBL.GTable = TBL.CreateTable();
            InitializeComponent();
        }

        private void Open_Click(object sender, EventArgs e)
        {
            List<DataBase> MyBase = new List<DataBase>();
            FindDocuments Finding = new FindDocuments();
            TBL.GTable.MyResult.Visible = false;
            TBL.GTable.Save.Visible = false;
            TBL.GTable.SaveSklad.Visible = false;
            TBL.GTable.Result.Visible = false;
            TBL.GTable.Plus.Visible = false;
            TBL.GTable.WithoutNalog.Visible = false;
            TBL.GTable.WithNalog.Visible = false;
            TBL.GTable.MyResult.Visible = false;
            TBL.GTable.Date.Visible = false;
            TBL.GTable.txtDate.Visible = false;
            TBL.GTable.buttonClear.Visible = false;
            TBL.GTable.lblPoints.Visible = false;
            TBL.GTable.txtPoints.Visible = false;
            if (txtDate.Text != "" && txtName.Text != "")
            {
                TBL.GTable.Show();
                string FileName = "";
                string[] FileNames = new string[2];
                string[] dirs = Directory.GetFiles(Environment.CurrentDirectory + "\\Накладные\\" + txtDate.Text);
                int interval = 1;
                int color = 0;
                foreach (string dir in dirs)
                {
                    if (dir.Contains(txtName.Text))
                    {
                        FileName = dir;
                        string area = Finding.GetData(FileName);
                        string[] areas = area.Split(':', ' ');
                        TBL.GTable.txtName.Text = areas[3];
                        TBL.GTable.txtNumber.Text = areas[6];
                        MyBase = Finding.ChooseDocument(FileName);
                        for (int i = 0; i < MyBase.Count; i++)
                        {
                            int n = TBL.GTable.Document1.RowCount;
                            var answer = MyBase.Find(x => x.Artikul.Contains(MyBase[i].Artikul.ToString()));
                            TBL.GTable.Document1["Number", i + interval].Value = answer.Artikul;
                            TBL.GTable.Document1["Product", i + interval].Value = answer.Name;
                            TBL.GTable.Document1["Sum", i + interval].Value = answer.buy100;
                            TBL.GTable.Document1["Count", i + interval].Value = answer.Count;
                            TBL.GTable.Document1["Points", i + interval].Value = answer.Ball;
                            TBL.GTable.Document1.Rows.Add();
                        }
                        TBL.GTable.Document1.Rows[color].Cells[1].Style.ForeColor = System.Drawing.Color.Red;
                        TBL.GTable.Document1.Rows[color].Cells[1].Style.Font = new System.Drawing.Font("Tahoma", 12);
                        TBL.GTable.Document1["Product", interval - 1].Value = areas[9] + " " + areas[10] + " " + areas[11];
                        interval = interval + MyBase.Count + 1;
                        color = interval - 1;

                    }
                }
            }
            else
            {
                MessageBox.Show("Необходимо заполнить все поля");
            }
            this.Hide();
        }

        private void MonthBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> Names = new List<string>();
            NameBox.Items.Clear();
            for (int i = 0; i < MonthBox.Items.Count; i++)
            {
                if (MonthBox.SelectedIndex == i) txtDate.Text = MonthBox.Items[i].ToString();
            }
            string[] dirs = Directory.GetFiles(Environment.CurrentDirectory + "\\Накладные\\" + txtDate.Text);
            foreach (string dir in dirs)
            {
                
                    string[] finding = dir.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                    string name = finding[finding.Count() - 1];
                    string result = "";
                    for (int i = 0; i < name.Length; i++)
                    {
                        if (name[i] != '_')
                        {
                            result = result + name[i];
                        }
                        else break;
                    }                    
                    if (!Names.Contains(result))
                    {
                        Names.Add(result);
                        NameBox.Items.Add(result);
                    }
                
            }
        }

        private void NameBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            for (int i = 0; i < NameBox.Items.Count; i++)
            {
                if (NameBox.SelectedIndex == i) txtName.Text = NameBox.Items[i].ToString();
            }
        }

        private void OpenDoc_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Hide();
                Start t = new Start();
                t.Show();
            }
        }
    }
}
