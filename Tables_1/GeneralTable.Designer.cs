﻿namespace Tables_1
{
    public partial class FormGeneralTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        //private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGeneralTable));
            this.Document1 = new System.Windows.Forms.DataGridView();
            this.lblName = new System.Windows.Forms.Label();
            this.Date = new System.Windows.Forms.Label();
            this.Number = new System.Windows.Forms.Label();
            this.Save = new System.Windows.Forms.Button();
            this.SaveSklad = new System.Windows.Forms.Button();
            this.txtDiscount = new System.Windows.Forms.Label();
            this.Result = new System.Windows.Forms.Label();
            this.WithoutNalog = new System.Windows.Forms.TextBox();
            this.Plus = new System.Windows.Forms.Label();
            this.WithNalog = new System.Windows.Forms.TextBox();
            this.MyResult = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.Goods = new System.Windows.Forms.Label();
            this.lblPoints = new System.Windows.Forms.Label();
            this.txtPoints = new System.Windows.Forms.TextBox();
            this.txtNumber = new System.Windows.Forms.MaskedTextBox();
            this.txtName = new System.Windows.Forms.MaskedTextBox();
            this.txtDate = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.Document1)).BeginInit();
            this.SuspendLayout();
            // 
            // Document1
            // 
            this.Document1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Document1.Location = new System.Drawing.Point(52, 121);
            this.Document1.Name = "Document1";
            this.Document1.Size = new System.Drawing.Size(1061, 363);
            this.Document1.TabIndex = 0;
            this.Document1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Document1_KeyUp);
            // 
            // lblName
            // 
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblName.ForeColor = System.Drawing.Color.Navy;
            this.lblName.Location = new System.Drawing.Point(47, 53);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(76, 37);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "ФИО:";
            // 
            // Date
            // 
            this.Date.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Date.ForeColor = System.Drawing.Color.Navy;
            this.Date.Location = new System.Drawing.Point(767, 50);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(76, 37);
            this.Date.TabIndex = 2;
            this.Date.Text = "Дата:";
            // 
            // Number
            // 
            this.Number.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Number.ForeColor = System.Drawing.Color.Navy;
            this.Number.Location = new System.Drawing.Point(398, 53);
            this.Number.Name = "Number";
            this.Number.Size = new System.Drawing.Size(126, 37);
            this.Number.TabIndex = 3;
            this.Number.Text = "Номер:";
            // 
            // Save
            // 
            this.Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Save.ForeColor = System.Drawing.Color.Navy;
            this.Save.Location = new System.Drawing.Point(57, 661);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(261, 61);
            this.Save.TabIndex = 7;
            this.Save.Text = "Сохранить";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Visible = false;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // SaveSklad
            // 
            this.SaveSklad.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.SaveSklad.ForeColor = System.Drawing.Color.Navy;
            this.SaveSklad.Location = new System.Drawing.Point(448, 661);
            this.SaveSklad.Name = "SaveSklad";
            this.SaveSklad.Size = new System.Drawing.Size(261, 61);
            this.SaveSklad.TabIndex = 9;
            this.SaveSklad.Text = "Сохранить";
            this.SaveSklad.UseVisualStyleBackColor = true;
            this.SaveSklad.Visible = false;
            this.SaveSklad.Click += new System.EventHandler(this.SaveSklad_Click);
            // 
            // txtDiscount
            // 
            this.txtDiscount.AutoSize = true;
            this.txtDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtDiscount.Location = new System.Drawing.Point(422, 618);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(0, 25);
            this.txtDiscount.TabIndex = 10;
            // 
            // Result
            // 
            this.Result.AutoSize = true;
            this.Result.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Result.ForeColor = System.Drawing.Color.Navy;
            this.Result.Location = new System.Drawing.Point(52, 526);
            this.Result.Name = "Result";
            this.Result.Size = new System.Drawing.Size(68, 25);
            this.Result.TabIndex = 11;
            this.Result.Text = "Итог:";
            // 
            // WithoutNalog
            // 
            this.WithoutNalog.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.WithoutNalog.Location = new System.Drawing.Point(158, 521);
            this.WithoutNalog.Multiline = true;
            this.WithoutNalog.Name = "WithoutNalog";
            this.WithoutNalog.ReadOnly = true;
            this.WithoutNalog.Size = new System.Drawing.Size(221, 37);
            this.WithoutNalog.TabIndex = 12;
            // 
            // Plus
            // 
            this.Plus.AutoSize = true;
            this.Plus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Plus.ForeColor = System.Drawing.Color.Navy;
            this.Plus.Location = new System.Drawing.Point(422, 526);
            this.Plus.Name = "Plus";
            this.Plus.Size = new System.Drawing.Size(120, 25);
            this.Plus.TabIndex = 13;
            this.Plus.Text = "+  5  %   =";
            // 
            // WithNalog
            // 
            this.WithNalog.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.WithNalog.Location = new System.Drawing.Point(574, 521);
            this.WithNalog.Multiline = true;
            this.WithNalog.Name = "WithNalog";
            this.WithNalog.ReadOnly = true;
            this.WithNalog.Size = new System.Drawing.Size(209, 37);
            this.WithNalog.TabIndex = 14;
            // 
            // MyResult
            // 
            this.MyResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.MyResult.ForeColor = System.Drawing.Color.Navy;
            this.MyResult.Location = new System.Drawing.Point(843, 582);
            this.MyResult.Name = "MyResult";
            this.MyResult.Size = new System.Drawing.Size(261, 61);
            this.MyResult.TabIndex = 15;
            this.MyResult.Text = "Посчитать";
            this.MyResult.UseVisualStyleBackColor = true;
            this.MyResult.Click += new System.EventHandler(this.MyResult_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.buttonClear.ForeColor = System.Drawing.Color.Navy;
            this.buttonClear.Location = new System.Drawing.Point(843, 661);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(261, 61);
            this.buttonClear.TabIndex = 16;
            this.buttonClear.Text = "Очистить";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // Goods
            // 
            this.Goods.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Goods.Location = new System.Drawing.Point(52, 78);
            this.Goods.Name = "Goods";
            this.Goods.Size = new System.Drawing.Size(166, 23);
            this.Goods.TabIndex = 17;
            this.Goods.Visible = false;
            // 
            // lblPoints
            // 
            this.lblPoints.AutoSize = true;
            this.lblPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblPoints.ForeColor = System.Drawing.Color.Navy;
            this.lblPoints.Location = new System.Drawing.Point(55, 600);
            this.lblPoints.Name = "lblPoints";
            this.lblPoints.Size = new System.Drawing.Size(89, 25);
            this.lblPoints.TabIndex = 20;
            this.lblPoints.Text = "Баллы:";
            // 
            // txtPoints
            // 
            this.txtPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtPoints.Location = new System.Drawing.Point(158, 588);
            this.txtPoints.Multiline = true;
            this.txtPoints.Name = "txtPoints";
            this.txtPoints.ReadOnly = true;
            this.txtPoints.Size = new System.Drawing.Size(221, 37);
            this.txtPoints.TabIndex = 21;
            // 
            // txtNumber
            // 
            this.txtNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtNumber.Location = new System.Drawing.Point(530, 47);
            this.txtNumber.Mask = "0000000";
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(164, 31);
            this.txtNumber.TabIndex = 22;
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtName.Location = new System.Drawing.Point(142, 47);
            this.txtName.Mask = "L/L/L???????????";
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(165, 31);
            this.txtName.TabIndex = 23;
            // 
            // txtDate
            // 
            this.txtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtDate.Location = new System.Drawing.Point(872, 47);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(241, 31);
            this.txtDate.TabIndex = 24;
            // 
            // FormGeneralTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1183, 761);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtNumber);
            this.Controls.Add(this.txtPoints);
            this.Controls.Add(this.lblPoints);
            this.Controls.Add(this.Goods);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.MyResult);
            this.Controls.Add(this.WithNalog);
            this.Controls.Add(this.Plus);
            this.Controls.Add(this.WithoutNalog);
            this.Controls.Add(this.Result);
            this.Controls.Add(this.txtDiscount);
            this.Controls.Add(this.SaveSklad);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.Number);
            this.Controls.Add(this.Date);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.Document1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormGeneralTable";
            this.Text = "Таблица";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormGeneralTable_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.Document1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lblName;
        public System.Windows.Forms.Label Date;
        public System.Windows.Forms.Label Number;
        public System.Windows.Forms.Button Save;
        public System.Windows.Forms.Button SaveSklad;
        public System.Windows.Forms.Label txtDiscount;
        public System.Windows.Forms.DataGridView Document1;
        public System.Windows.Forms.TextBox WithoutNalog;
        public System.Windows.Forms.TextBox WithNalog;
        public System.Windows.Forms.Label Result;
        public System.Windows.Forms.Label Plus;
        public System.Windows.Forms.Button MyResult;
        public System.Windows.Forms.Label Goods;
        public System.Windows.Forms.Button buttonClear;
        public System.Windows.Forms.Label lblPoints;
        public System.Windows.Forms.TextBox txtPoints;
        public System.Windows.Forms.MaskedTextBox txtNumber;
        public System.Windows.Forms.MaskedTextBox txtName;
        public System.Windows.Forms.DateTimePicker txtDate;
    }
}