﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Word;
using System.IO;

namespace Tables_1
{
    #region Interfaces
    public interface ICreateTable
    {
        FormGeneralTable CreateTable();
        List<DataBase> ReadingCatalog();
    }

    public interface ISavingInformation
    {
        void CreateFile(List<DataBase> Doc, string name, string number, string date, string result, string points);
    }

    public interface IArrears
    {
        void Arrears();
        void ListOfArrears(List<DataBase> Doc, string name, string number, string date, string result);
        List<NPA> GetArrears();
    }

    public interface IAmount
    {
        double Summa(List<DataBase> Doc);
        double DeliveryPrice();
    }

    public interface IReturns
    {
        void ReturnsTable(string txtNumber, string txtName, string Summa, string RealSumma, string ADiscount, string Discount, string Return, string txtData);
    }


    public interface IDocuments
    {
        List<DataBase> ChooseDocument(string Document);
        string GetData(string Document);
    }

    public interface IStorage
    {
        void MyBase();
        List<DataBase> AddGoods();
        void SaveIt(List<DataBase> Base);
    }

    public interface IReadInfo
    {
        void Reading();
    }

    #endregion

    #region Classes
    public class DataBase
    {
        public string Artikul { get; set; }
        public string Name { get; set; }
        public string Count { get; set; }
        public decimal buy100 { get; set; }
        public decimal buy15 { get; set; }
        public decimal buy23 { get; set; }
        public decimal BO { get; set; }
        public decimal Ball { get; set; }
        public int Sklad { get; set; }
        public int Arrear { get; set; }

        public DataBase(string A, string N, string C, decimal B100, decimal B15, decimal B23, decimal bo, decimal B, int S, int Arr)
        {
            Artikul = A;
            Name = N;
            Count = C;
            buy100 = B100;
            buy15 = B15;
            buy23 = B23;
            BO = bo;
            Ball = B;
            Sklad = S;
            Arrear = Arr;
        }
    }

    public class NPA
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string Artikul { get; set; }
        public string Product { get; set; }
        public int Arrear { get; set; }

        public NPA(string N, string name, string art, string prod, int count)
        {
            Number = N;
            Name = name;
            Artikul = art;
            Product = prod;
            Arrear = count;
        }
    }
   

    #region Создание таблицы
    public class Table1 : ICreateTable
    {
        public FormGeneralTable GTable = new FormGeneralTable();
        public Creator creator = new Creator();
        public List<DataBase> MyBase = new List<DataBase>();
        //public string[] Buy = new string[8];
        public string[] Buy = new string[10];
        public FormGeneralTable CreateTable()
        {
            creator.Create();
            creator.column1.HeaderText = "Артикул"; //текст в шапке
            creator.column1.Width = 100; //ширина колонки
            creator.column1.Name = "Number"; //текстовое имя колонки, его можно использовать вместо обращений по индексу
            creator.column1.Frozen = true; //флаг, что данная колонка всегда отображается на своем месте
            

            creator.column2.HeaderText = "Продукция";
            creator.column2.Width = 400; //ширина колонки
            creator.column2.Name = "Product";
            creator.column2.ReadOnly = true;


            creator.column3.HeaderText = "Количество";
            creator.column3.Name = "Count";


            creator.column4.HeaderText = "Сумма";
            creator.column4.Name = "Sum";
            creator.column4.ReadOnly = true;


            creator.column5.HeaderText = "Баллы";
            creator.column5.Name = "Points";
            creator.column5.ReadOnly = true;


            creator.column6.HeaderText = "Склад";
            creator.column6.Name = "Storage";
            creator.column6.ReadOnly = true;

            creator.column7.HeaderText = "Долги";
            creator.column7.Name = "Arrears";
            //creator.column7.ReadOnly = true;


            GTable.Document1.Columns.Add(creator.column1);
            GTable.Document1.Columns.Add(creator.column2);
            GTable.Document1.Columns.Add(creator.column3);
            GTable.Document1.Columns.Add(creator.column4);
            GTable.Document1.Columns.Add(creator.column5);
            GTable.Document1.Columns.Add(creator.column6);
            GTable.Document1.Columns.Add(creator.column7);

            for (int i = 0; i < 15; ++i)
            {
                GTable.Document1.Rows.Add();
            }
            return GTable;
        }
        public List<DataBase> ReadingCatalog()
        {
            using (WordprocessingDocument doc = WordprocessingDocument.
                            Open("t1.docx", false))
            {
                DocumentFormat.OpenXml.Wordprocessing.Table toxmlTable = doc.MainDocumentPart.Document.Body.
                    Elements<DocumentFormat.OpenXml.Wordprocessing.Table>().First();

              
                int i = 0;
                foreach (TableRow row in toxmlTable.Elements<TableRow>())
                {
                    foreach (TableCell cell in row.Elements<TableCell>())
                    {
                        if (cell.InnerText != "")
                        {
                            Buy[i] = cell.InnerText;
                            i++;
                        }
                    }
                    i = 0;
                    if (Buy[0] != "")
                        MyBase.Add(new DataBase(Buy[0], Buy[1], Buy[2], decimal.Parse(Buy[3]), decimal.Parse(Buy[4]), decimal.Parse(Buy[5]), decimal.Parse(Buy[6]), decimal.Parse(Buy[7]), int.Parse(Buy[8]), 0));
                    for (int j = 0; j < Buy.Length; j++)
                    {
                        Buy[j] = "";
                    }
                    Console.WriteLine();
                }
            }
            return MyBase;
        }
    }
    #endregion

    #region Склад
    public class Storage : IStorage
    {
        public FormGeneralTable GTable = new FormGeneralTable();
        public Creator creator = new Creator();
        public string[] Buy = new string[9];
        public List<DataBase> Base = new List<DataBase>();
        public void MyBase()
        {
            creator.Create();
            creator.column1.HeaderText = "Артикул"; //текст в шапке
            creator.column1.Width = 200; //ширина колонки
            creator.column1.Name = "Number"; //текстовое имя колонки, его можно использовать вместо обращений по индексу
            creator.column1.Frozen = true; //флаг, что данная колонка всегда отображается на своем месте


            creator.column2.HeaderText = "Продукция";
            creator.column2.Width = 600; //ширина колонки
            creator.column2.Name = "Product";
            creator.column2.ReadOnly = true;


            creator.column6.HeaderText = "Склад";
            creator.column6.Width = 200;
            creator.column6.Name = "Count";


            GTable.Document1.Columns.Add(creator.column1);
            GTable.Document1.Columns.Add(creator.column2);
            GTable.Document1.Columns.Add(creator.column6);

            for (int i = 0; i < 15; ++i)
            {
                GTable.Document1.Rows.Add();
            }
        }
        public List<DataBase> AddGoods()
        {
            using (WordprocessingDocument doc = WordprocessingDocument.
                            Open("t1.docx", false))
            {
                DocumentFormat.OpenXml.Wordprocessing.Table toxmlTable = doc.MainDocumentPart.Document.Body.
                    Elements<DocumentFormat.OpenXml.Wordprocessing.Table>().First();


                int i = 0;
                foreach (TableRow row in toxmlTable.Elements<TableRow>())
                {
                    foreach (TableCell cell in row.Elements<TableCell>())
                    {
                        if (cell.InnerText != "")
                        {
                            Buy[i] = cell.InnerText;
                            i++;
                        }
                    }
                    i = 0;
                    if (Buy[0] != "")
                        Base.Add(new DataBase(Buy[0], Buy[1], Buy[2], decimal.Parse(Buy[3]), decimal.Parse(Buy[4]), decimal.Parse(Buy[5]), decimal.Parse(Buy[6]), decimal.Parse(Buy[7]), int.Parse(Buy[8]), 0));
                    for (int j = 0; j < Buy.Length; j++)
                    {
                        Buy[j] = "";
                    }
                    Console.WriteLine();
                }
            }
            return Base;
        }

        public void SaveIt(List<DataBase> Base)
        {
            using (WordprocessingDocument doc = WordprocessingDocument.Open("t1.docx", true))
            {
                for (int i = 0; i < Base.Count; i++)
                {
                    // Find the first table in the document.
                    DocumentFormat.OpenXml.Wordprocessing.Table table =
                        doc.MainDocumentPart.Document.Body.Elements<DocumentFormat.OpenXml.Wordprocessing.Table>().First();

                    // Find the second row in the table.
                    TableRow row = table.Elements<TableRow>().ElementAt(i);

                    // Find the third cell in the row.
                    TableCell cell = row.Elements<TableCell>().ElementAt(8);

                    // Find the first paragraph in the table cell.
                    DocumentFormat.OpenXml.Wordprocessing.Paragraph p = cell.Elements<DocumentFormat.OpenXml.Wordprocessing.Paragraph>().First();

                    // Find the first run in the paragraph.
                    Run r = p.Elements<Run>().First();

                    // Set the text for the run.
                    Text t = r.Elements<Text>().First();
                    t.Text = Base[i].Sklad.ToString(); ;
                }
            }
        }
    }
    #endregion

    #region Сохранение 
    public class Saving : ISavingInformation
    {
        int counter = 0;
        int i = 1;
        Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();
        public void CreateFile(List<DataBase> Doc, string name, string number, string date, string result, string points)
        {
            
            //winword.Visible = false;

            object missing = System.Reflection.Missing.Value;

            //Создание нового документа
            Microsoft.Office.Interop.Word.Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);

            //Добавление текста 
            Microsoft.Office.Interop.Word.Paragraph para1 = document.Content.Paragraphs.Add(ref missing);
            para1.Range.Text = "ФИО: " + name;
            para1.Range.InsertParagraphAfter();

            Microsoft.Office.Interop.Word.Paragraph para2 = document.Content.Paragraphs.Add(ref missing);
            para2.Range.Text = "Номер: " + number;
            para2.Range.InsertParagraphAfter();

            Microsoft.Office.Interop.Word.Paragraph para3 = document.Content.Paragraphs.Add(ref missing);
            para3.Range.Text = "Дата: " + date;
            para3.Range.InsertParagraphAfter();

            //Создание таблицы 
            Microsoft.Office.Interop.Word.Table firstTable = document.Tables.Add(para1.Range, 2, 5, ref missing, ref missing);
            int count = 0;
            firstTable.Borders.Enable = 1;
            foreach (Row row in firstTable.Rows)
            {
                foreach (Cell cell in row.Cells)
                {
                    //Заголовок таблицы
                    if (cell.RowIndex == 1)
                    {
                        if (cell.ColumnIndex == 1) cell.Range.Text = "Артикул";
                        if (cell.ColumnIndex == 2) cell.Range.Text = "Продукция";
                        if (cell.ColumnIndex == 3) cell.Range.Text = "Сумма";
                        if (cell.ColumnIndex == 4) cell.Range.Text = "Количество";
                        if (cell.ColumnIndex == 5) cell.Range.Text = "Баллы";
                        cell.Range.Font.Bold = 1;
                        //Задаем шрифт и размер текста
                        cell.Range.Font.Name = "verdana";
                        cell.Range.Font.Size = 10;
                        cell.Shading.BackgroundPatternColor = WdColor.wdColorGray25;
                        //Выравнивание текста в заголовках столбцов по центру
                        cell.VerticalAlignment =
                        WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                        cell.Range.ParagraphFormat.Alignment =
                        WdParagraphAlignment.wdAlignParagraphCenter;
                       
                    }
                    //Значения ячеек
                    else
                    {
                        if (counter <= Doc.Count)
                        {
                            firstTable.Rows.Add(ref missing);//Добавляем в таблицу строку.
                            count++;
                            counter++;
                        }
                       if(i - 1 < Doc.Count)
                        {
                            int j = 1;
                            firstTable.Rows[i + 1].Cells[j].Range.Text = Doc[i - 1].Artikul;
                            firstTable.Rows[i + 1].Cells[j + 1].Range.Text = Doc[i - 1].Name;
                            if (Doc[i - 1].buy23 != 0)
                            {
                                firstTable.Rows[i + 1].Cells[j + 2].Range.Text = Doc[i - 1].buy23.ToString();
                            }
                            if (Doc[i - 1].buy15 != 0)
                            {
                                firstTable.Rows[i + 1].Cells[j + 2].Range.Text = Doc[i - 1].buy15.ToString();
                            }
                            if (Doc[i - 1].buy100 != 0)
                            {
                                firstTable.Rows[i + 1].Cells[j + 2].Range.Text = Doc[i - 1].buy100.ToString();
                            }
                            firstTable.Rows[i + 1].Cells[j + 3].Range.Text = Doc[i - 1].Count;
                            firstTable.Rows[i + 1].Cells[j + 4].Range.Text = Doc[i - 1].Ball.ToString();
                            i++;
                        }
                        else break;
                    }
                }
            }
            Microsoft.Office.Interop.Word.Paragraph para4 = document.Content.Paragraphs.Add(ref missing);
            para4.Range.InsertAfter("Итог: " + result + "  Баллы: " + points);
            string currentData = "";
            for (int dataCount = 0; dataCount < date.Length; dataCount++)
            {
                while (date[dataCount] != ' ') dataCount++;
                    while (date[dataCount + 1] != ' ')
                    {
                        currentData += date[dataCount].ToString();
                        dataCount++;
                    }
                    currentData += date[dataCount].ToString();
                    break;
            }
            string FileName = name + "_" + number + "_" + date;

            #region Выбор папки
            switch (currentData)
            {
                case " января":
                    document.SaveAs(Environment.CurrentDirectory + "\\Накладные\\Январь\\" + FileName + ".docx");
                    break;
                case " февраля":
                    document.SaveAs(Environment.CurrentDirectory + "\\Накладные\\Февраль\\" + FileName + ".docx");
                    break;
                case " марта":
                    document.SaveAs(Environment.CurrentDirectory + "\\Накладные\\Март\\" + FileName + ".docx");
                    break;
                case " апреля":
                    document.SaveAs(Environment.CurrentDirectory + "\\Накладные\\Апрель\\" + FileName + ".docx");
                    break;
                case " мая":
                    document.SaveAs(Environment.CurrentDirectory + "\\Накладные\\Май\\" + FileName + ".docx");
                    break;
                case " июня":
                    document.SaveAs(Environment.CurrentDirectory + "\\Накладные\\Июнь\\" + FileName + ".docx");
                    break;
                case " июля":
                    document.SaveAs(Environment.CurrentDirectory + "\\Накладные\\Иоль\\" + FileName + ".docx");
                    break;
                case " августа":
                    document.SaveAs(Environment.CurrentDirectory + "\\Накладные\\Август\\" + FileName + ".docx");
                    break;
                case " сентября":
                    document.SaveAs(Environment.CurrentDirectory + "\\Накладные\\Сентябрь\\" + FileName + ".docx");
                    break;
                case " октября":
                    document.SaveAs(Environment.CurrentDirectory + "\\Накладные\\Октябрь\\" + FileName + ".docx");
                    break;
                case " ноября":
                    document.SaveAs(Environment.CurrentDirectory + "\\Накладные\\Ноябрь\\" + FileName + ".docx");
                    break;
                case " декабря":
                    document.SaveAs(Environment.CurrentDirectory + "\\Накладные\\Декабрь\\" + FileName + ".docx");
                    break;
            }
            #endregion

            document.Close();
        }
        public void Clear() { }
    }
    #endregion

    #region Список долгов
    public class ListArrears : IArrears
    {
        public FormGeneralTable GTable = new FormGeneralTable();
        public Creator creator = new Creator();
        public List<NPA> MyBase = new List<NPA>();
        public string[] Buy = new string[5];
        Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();
        int counter = 0;
        int i = 1;
        int k = 0;
        public void Arrears()
        {
            creator.Create();
            creator.column1.HeaderText = "Номер НПА"; //текст в шапке
            creator.column1.Width = 125; //ширина колонки
            creator.column1.Name = "Number"; //текстовое имя колонки, его можно использовать вместо обращений по индексу
            creator.column1.Frozen = true; //флаг, что данная колонка всегда отображается на своем месте
            creator.column1.ReadOnly = true;

            creator.column2.HeaderText = "ФИО";
            creator.column2.Width = 180; //ширина колонки
            creator.column2.Name = "Name";
            creator.column2.ReadOnly = true;


            creator.column3.HeaderText = "Артикул";
            creator.column3.Width = 145;
            creator.column3.Name = "Artikul";
            creator.column3.ReadOnly = true;

            creator.column4.HeaderText = "Продукция";
            creator.column4.Width = 405;
            creator.column4.Name = "Product";
            creator.column4.ReadOnly = true;


            creator.column5.HeaderText = "Количество";
            creator.column5.Width = 145;
            creator.column5.Name = "Count";
            creator.column5.ReadOnly = true;


            GTable.Document1.Columns.Add(creator.column1);
            GTable.Document1.Columns.Add(creator.column2);
            GTable.Document1.Columns.Add(creator.column3);
            GTable.Document1.Columns.Add(creator.column4);
            GTable.Document1.Columns.Add(creator.column5);

            for (int i = 0; i < 15; ++i)
            {
                GTable.Document1.Rows.Add();
            }
        }

        public void ListOfArrears(List<DataBase> Doc, string name, string number, string date, string result)
        {
            object missing = System.Reflection.Missing.Value;

            Microsoft.Office.Interop.Word.Document document = winword.Documents.Open(Path.Combine(Environment.CurrentDirectory, "Долги.docx"), ReadOnly: false);
         
            //Добавление текста 
            Microsoft.Office.Interop.Word.Paragraph para1 = document.Content.Paragraphs.Add(ref missing);

            //Создание таблицы 
            Microsoft.Office.Interop.Word.Table firstTable1 = document.Tables.Add(para1.Range, 2, 5, ref missing, ref missing);

            Microsoft.Office.Interop.Word.Table firstTable = winword.ActiveDocument.Tables[1];
            int count = 0;
            firstTable.Borders.Enable = 1;
            foreach (Row row in firstTable.Rows)
            {
                foreach (Cell cell in row.Cells)
                {
                    //Заголовок таблицы
                    if (cell.RowIndex == 1)
                    {
                        if (cell.ColumnIndex == 1) cell.Range.Text = "№НПА";
                        if (cell.ColumnIndex == 2) cell.Range.Text = "ФИО";
                        if (cell.ColumnIndex == 3) cell.Range.Text = "Артикул";
                        if (cell.ColumnIndex == 4) cell.Range.Text = "Продукция";
                        if (cell.ColumnIndex == 5) cell.Range.Text = "Долг";
                        cell.Range.Font.Bold = 1;
                        //Задаем шрифт и размер текста
                        cell.Range.Font.Name = "verdana";
                        cell.Range.Font.Size = 10;
                        cell.Shading.BackgroundPatternColor = WdColor.wdColorGray25;
                        //Выравнивание текста в заголовках столбцов по центру
                        cell.VerticalAlignment =
                        WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                        cell.Range.ParagraphFormat.Alignment =
                        WdParagraphAlignment.wdAlignParagraphCenter;

                    }
                    //Значения ячеек
                    else
                    {
                        if (i - 1 < Doc.Count && Doc[k].Arrear !=0)
                        {
                            firstTable.Rows.Add(ref missing);//Добавляем в таблицу строку.
                            count++;
                            counter++;
                            int j = 1;
                            while (firstTable.Rows[i + 1].Cells[j].Range.Text != "\r\a") i++;
                            firstTable.Rows[i + 1].Cells[j].Range.Text = number;
                            firstTable.Rows[i + 1].Cells[j + 1].Range.Text = name;
                            firstTable.Rows[i + 1].Cells[j + 2].Range.Text = Doc[k].Artikul;
                            firstTable.Rows[i + 1].Cells[j + 3].Range.Text = Doc[k].Name;
                            firstTable.Rows[i + 1].Cells[j + 4].Range.Text = Doc[k].Arrear.ToString();
                            i++;
                            k++;
                        }
                        else break;
                    }
                }
            }
            string savedFileName =  "Долги.docx";
            document.SaveAs(Path.Combine(Environment.CurrentDirectory, savedFileName));
            document.Close();
        }

        public List<NPA> GetArrears()
        {
            using (WordprocessingDocument doc = WordprocessingDocument.
                            Open("Долги.docx", false))
            {
                DocumentFormat.OpenXml.Wordprocessing.Table toxmlTable = doc.MainDocumentPart.Document.Body.
                    Elements<DocumentFormat.OpenXml.Wordprocessing.Table>().First();


                int i = 0;
                foreach (TableRow row in toxmlTable.Elements<TableRow>())
                {
                    foreach (TableCell cell in row.Elements<TableCell>())
                    {
                        if (cell.InnerText != "")
                        {
                            Buy[i] = cell.InnerText;
                            i++;
                        }
                    }
                    i = 0;
                    if (Buy[0] != "" && Buy[0] != "№НПА")
                        MyBase.Add(new NPA(Buy[0], Buy[1], Buy[2], Buy[3],  int.Parse(Buy[4])));
                    for (int j = 0; j < Buy.Length; j++)
                    {
                        Buy[j] = "";
                    }
                    Console.WriteLine();
                }
            }
            return MyBase;
        }
    }
    #endregion

    #region Возвраты
    public class Returns : IReturns
    {
        private Microsoft.Office.Interop.Excel.Application application;
        private Workbook workBook;
        private Worksheet worksheet;
        int i = 3;
        public void ReturnsTable(string txtNumber, string txtName, string Summa, string RealSumma, string ADiscount, string Discount, string Return, string txtData)
        {
            // Открываем приложение
            application = new Microsoft.Office.Interop.Excel.Application
            {
                DisplayAlerts = false
            };
            string template = null;
            int counter = 0;
            string[] dirs = Directory.GetFiles(Environment.CurrentDirectory);
            foreach (string dir in dirs)
            {
                if (dir.Contains(txtData))
                {
                    // Файл шаблона
                    template = "Возвраты от " + txtData + ".xlsx";
                    counter++;
                }
            }
            if (counter == 0) template = "book.xlsx";

            // Открываем книгу
            workBook = application.Workbooks.Open(Path.Combine(Environment.CurrentDirectory, template));

            // Получаем активную таблицу
            worksheet = workBook.ActiveSheet as Worksheet;

            // Записываем данные
            if (worksheet.Range["B1"].Value == null)
            {
                worksheet.Range["B1"].Value = txtData;
                worksheet.Range["A2"].Value = "Номер";
                worksheet.Range["B2"].Value = "ФИО";
                worksheet.Range["C2"].Value = "Сдано";
                worksheet.Range["D2"].Value = "Сумма";
                worksheet.Range["E2"].Value = "Активная скидка";
                worksheet.Range["F2"].Value = "Скидка";
                worksheet.Range["G2"].Value = "Возврат";
            }

            while (worksheet.Cells[i, 1].Value != null) i++;
            worksheet.Cells[i, 1].Value = txtNumber;
            worksheet.Cells[i, 2].Value = txtName;
            worksheet.Cells[i, 3].Value = Summa;
            worksheet.Cells[i, 4].Value = RealSumma;
            worksheet.Cells[i, 5].Value = ADiscount;
            worksheet.Cells[i, 6].Value = Discount;
            worksheet.Cells[i, 7].Value = Return;

            // Показываем приложение
           // application.Visible = true;
            string savedFileName = "Возвраты от " + txtData + ".xlsx";
            workBook.SaveAs(Path.Combine(Environment.CurrentDirectory, savedFileName));
            workBook.Close();
        }
    }
    #endregion

    #region Поиск накладной
    public class FindDocuments : IDocuments
    {
        public List<DataBase> MyBase = new List<DataBase>();
        public string[] Buy = new string[5];
        public string GetData(string Document)
        {
            string result = "";
            using (WordprocessingDocument doc = WordprocessingDocument.
                            Open(Document, false))
            {
                DocumentFormat.OpenXml.Wordprocessing.Table toxmlTable = doc.MainDocumentPart.Document.Body.
                    Elements<DocumentFormat.OpenXml.Wordprocessing.Table>().First();

                foreach (DocumentFormat.OpenXml.Wordprocessing.Paragraph p in doc.MainDocumentPart.Document.Body.
                    Elements<DocumentFormat.OpenXml.Wordprocessing.Paragraph>())
                {
                    result = result + " " + p.InnerText;
                }
            }
            return result;
        }
        public List<DataBase> ChooseDocument(string Document)
        {
            MyBase.Clear();
            using (WordprocessingDocument doc = WordprocessingDocument.
                            Open(Document, false))
            {
                DocumentFormat.OpenXml.Wordprocessing.Table toxmlTable = doc.MainDocumentPart.Document.Body.
                    Elements<DocumentFormat.OpenXml.Wordprocessing.Table>().First();
                int i = 0;
                foreach (TableRow row in toxmlTable.Elements<TableRow>())
                {
                    foreach (TableCell cell in row.Elements<TableCell>())
                    {
                        if (cell.InnerText != "")
                        {
                            Buy[i] = cell.InnerText;
                            i++;
                        }
                    }
                    i = 0;
                    if (Buy[0] != "" && Buy[0] != "Артикул")
                        MyBase.Add(new DataBase(Buy[0], Buy[1], Buy[3], decimal.Parse(Buy[2]), 0, 0, 0, decimal.Parse(Buy[4]), 0, 0));
                    for (int j = 0; j < Buy.Length; j++)
                    {
                        Buy[j] = "";
                    }
                    Console.WriteLine();
                }
            }
            return MyBase;
        }
        public void Clear() { }
    }
    #endregion

    #region Скидки
    public class Amount : IAmount
    {
        public List<DataBase> Doc;
        public double Summa(List<DataBase> Doc) 
        {
            double sum1 = 0;
            for (int i = 0; i < Doc.Count; i++)
            {
                if(Doc[i].buy23 != 0)
                    sum1 += Convert.ToDouble(Doc[i].buy23);
                if (Doc[i].buy15 != 0)
                    sum1 += Convert.ToDouble(Doc[i].buy15);
                if (Doc[i].buy100 != 0)
                    sum1 += Convert.ToDouble(Doc[i].buy100);
            }
            return sum1;
        }
        public double DeliveryPrice() { return 0; }
        public int RoundTheAmount() { return 500; }
    }
    #endregion

    #region Документация
    public class Information : IReadInfo
    {
        Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();
        public void Reading()
        {
            Microsoft.Office.Interop.Word.Document document = winword.Documents.Open(Path.Combine(Environment.CurrentDirectory, "Info.docx"), ReadOnly: true);
            winword.Visible = true;
        }
    }
    #endregion

    #endregion
}
