﻿namespace Tables_1
{
    partial class CreateDoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateDoc));
            this.NameLbl = new System.Windows.Forms.Label();
            this.Number = new System.Windows.Forms.Label();
            this.Summa = new System.Windows.Forms.TextBox();
            this.RealSumma = new System.Windows.Forms.TextBox();
            this.ADiscount = new System.Windows.Forms.TextBox();
            this.Discount = new System.Windows.Forms.TextBox();
            this.Return = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Save = new System.Windows.Forms.Button();
            this.Calculate = new System.Windows.Forms.Button();
            this.Clear = new System.Windows.Forms.Button();
            this.lblData = new System.Windows.Forms.Label();
            this.txtNumber = new System.Windows.Forms.MaskedTextBox();
            this.txtName = new System.Windows.Forms.MaskedTextBox();
            this.txtData = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // NameLbl
            // 
            this.NameLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.NameLbl.ForeColor = System.Drawing.Color.Navy;
            this.NameLbl.Location = new System.Drawing.Point(27, 37);
            this.NameLbl.Name = "NameLbl";
            this.NameLbl.Size = new System.Drawing.Size(76, 37);
            this.NameLbl.TabIndex = 2;
            this.NameLbl.Text = "ФИО:";
            // 
            // Number
            // 
            this.Number.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Number.ForeColor = System.Drawing.Color.Navy;
            this.Number.Location = new System.Drawing.Point(311, 37);
            this.Number.Name = "Number";
            this.Number.Size = new System.Drawing.Size(126, 37);
            this.Number.TabIndex = 6;
            this.Number.Text = "Номер:";
            // 
            // Summa
            // 
            this.Summa.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Summa.ForeColor = System.Drawing.Color.Black;
            this.Summa.Location = new System.Drawing.Point(205, 204);
            this.Summa.Multiline = true;
            this.Summa.Name = "Summa";
            this.Summa.Size = new System.Drawing.Size(91, 37);
            this.Summa.TabIndex = 11;
            // 
            // RealSumma
            // 
            this.RealSumma.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RealSumma.ForeColor = System.Drawing.Color.Black;
            this.RealSumma.Location = new System.Drawing.Point(205, 247);
            this.RealSumma.Multiline = true;
            this.RealSumma.Name = "RealSumma";
            this.RealSumma.Size = new System.Drawing.Size(91, 37);
            this.RealSumma.TabIndex = 13;
            // 
            // ADiscount
            // 
            this.ADiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ADiscount.ForeColor = System.Drawing.Color.Black;
            this.ADiscount.Location = new System.Drawing.Point(205, 290);
            this.ADiscount.Multiline = true;
            this.ADiscount.Name = "ADiscount";
            this.ADiscount.Size = new System.Drawing.Size(91, 37);
            this.ADiscount.TabIndex = 15;
            // 
            // Discount
            // 
            this.Discount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Discount.ForeColor = System.Drawing.Color.Black;
            this.Discount.Location = new System.Drawing.Point(205, 333);
            this.Discount.Multiline = true;
            this.Discount.Name = "Discount";
            this.Discount.Size = new System.Drawing.Size(91, 37);
            this.Discount.TabIndex = 17;
            // 
            // Return
            // 
            this.Return.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Return.Location = new System.Drawing.Point(458, 200);
            this.Return.Multiline = true;
            this.Return.Name = "Return";
            this.Return.ReadOnly = true;
            this.Return.Size = new System.Drawing.Size(91, 37);
            this.Return.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(28, 204);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 37);
            this.label8.TabIndex = 24;
            this.label8.Text = "Сдано";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(27, 251);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(140, 37);
            this.label9.TabIndex = 25;
            this.label9.Text = "Сумма заказа";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.Navy;
            this.label10.Location = new System.Drawing.Point(27, 294);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(167, 37);
            this.label10.TabIndex = 26;
            this.label10.Text = "Активная скидка";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(27, 331);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 37);
            this.label11.TabIndex = 27;
            this.label11.Text = "Скидка";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(361, 204);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 37);
            this.label14.TabIndex = 30;
            this.label14.Text = "Возврат";
            // 
            // Save
            // 
            this.Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Save.ForeColor = System.Drawing.Color.Navy;
            this.Save.Location = new System.Drawing.Point(345, 290);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(204, 67);
            this.Save.TabIndex = 31;
            this.Save.Text = "Сохранить";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // Calculate
            // 
            this.Calculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Calculate.ForeColor = System.Drawing.Color.Navy;
            this.Calculate.Location = new System.Drawing.Point(345, 407);
            this.Calculate.Name = "Calculate";
            this.Calculate.Size = new System.Drawing.Size(199, 67);
            this.Calculate.TabIndex = 32;
            this.Calculate.Text = "Посчитать";
            this.Calculate.UseVisualStyleBackColor = true;
            this.Calculate.Click += new System.EventHandler(this.Calculate_Click);
            // 
            // Clear
            // 
            this.Clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Clear.ForeColor = System.Drawing.Color.Navy;
            this.Clear.Location = new System.Drawing.Point(44, 407);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(207, 67);
            this.Clear.TabIndex = 33;
            this.Clear.Text = "Очистить";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // lblData
            // 
            this.lblData.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblData.ForeColor = System.Drawing.Color.Navy;
            this.lblData.Location = new System.Drawing.Point(27, 99);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(126, 37);
            this.lblData.TabIndex = 34;
            this.lblData.Text = "Дата:";
            // 
            // txtNumber
            // 
            this.txtNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtNumber.Location = new System.Drawing.Point(443, 34);
            this.txtNumber.Mask = "0000000";
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(167, 31);
            this.txtNumber.TabIndex = 36;
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtName.Location = new System.Drawing.Point(109, 34);
            this.txtName.Mask = "L/L/????????????";
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(167, 31);
            this.txtName.TabIndex = 37;
            // 
            // txtData
            // 
            this.txtData.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtData.Location = new System.Drawing.Point(109, 94);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(240, 31);
            this.txtData.TabIndex = 38;
            // 
            // CreateDoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(662, 525);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtNumber);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.Calculate);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Return);
            this.Controls.Add(this.Discount);
            this.Controls.Add(this.ADiscount);
            this.Controls.Add(this.RealSumma);
            this.Controls.Add(this.Summa);
            this.Controls.Add(this.Number);
            this.Controls.Add(this.NameLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CreateDoc";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Returns_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label NameLbl;
        public System.Windows.Forms.Label Number;
        public System.Windows.Forms.TextBox Summa;
        public System.Windows.Forms.TextBox RealSumma;
        public System.Windows.Forms.TextBox ADiscount;
        public System.Windows.Forms.TextBox Discount;
        public System.Windows.Forms.TextBox Return;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Button Save;
        public System.Windows.Forms.Button Calculate;
        public System.Windows.Forms.Button Clear;
        public System.Windows.Forms.Label lblData;
        private System.Windows.Forms.MaskedTextBox txtNumber;
        private System.Windows.Forms.MaskedTextBox txtName;
        private System.Windows.Forms.DateTimePicker txtData;
    }
}